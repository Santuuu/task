import React from 'react'
import MenuScreen from './MenuScreen';

const BaseScreen = ({
    className = "p-4",
    children
}) => {
    return (
        <div>
            <MenuScreen/>
            <div className="container-fluid">
                <div className={className}>{children}</div>
            </div>
            <footer className="footer ">
                <div className="container-fluid text-center py-3">
                    <p>If you got any questions, feel free to reach out!</p>
                    <h5 className="text-muted text-center">
                     Shopping Store
                    </h5>
                </div>
            </footer>
        </div>
    )
}
export default BaseScreen