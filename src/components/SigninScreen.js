import React, {useState} from 'react'
import BaseScreen from './BaseScreen';
import {Link} from 'react-router-dom'
import { Redirect } from "react-router-dom";
import { API } from "../backend";


const SigninScreen = () => {
    const [values, setValues] = useState({
        email: "b@gmail.com",
        password: "12345",
        error: "",
        loading: false,
        didRedirect: false
      });

      const { email, password, error, loading, didRedirect } = values;


      const handleChange = name => event => {
        setValues({ ...values, error: false, [name]: event.target.value });
      };
    
     const signin = user => {
        return fetch(`${API}/signin`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify(user)
        })
          .then(response => {
            return response.json();
          })
          .catch(err => console.log(err));
      };

      const authenticate = (data, next) => {
        if (typeof window !== "undefined") {
          localStorage.setItem("jwt", JSON.stringify(data));
          next();
        }
      };

      const isAutheticated = () => {
        if (typeof window == "undefined") {
          return false;
        }
        if (localStorage.getItem("jwt")) {
          return JSON.parse(localStorage.getItem("jwt"));
        } else {
          return false;
        }
      };

      const onSubmit = event => {
        event.preventDefault();
        setValues({ ...values, error: false, loading: true });
        signin({ email, password })
          .then(data => {
            if (data.error) {
              setValues({ ...values, error: data.error, loading: false });
            } else {
              authenticate(data, () => {
                setValues({
                  ...values,
                  didRedirect: true
                });
              });
            }
          })
          .catch(console.log("signin request failed"));
      };

      
      const { user } = isAutheticated();

      const performRedirect = () => {
        if (didRedirect) {
          if (user && user.role === 1) {
            return <Redirect to="/admin/dashboard" />;
          } else {
            return <Redirect to="/" />;
          }
        }
      }

      const loadingMessage = () => {
        return (
          loading && (
            <div className="alert alert-info">
              <h2>Loading...</h2>
            </div>
          )
        );
      };
    

      const errorMessage = () => {
        return (
          <div className="row">
            <div className="col-md-6 offset-sm-3 text-left">
              <div
                className="alert alert-danger"
                style={{ display: error ? "" : "none" }}
              >
                {error}
              </div>
            </div>
          </div>
        );
      };


    const signinForm = () => {
        return (
            <div className="container mt-1">
                <div className='row justify-content-md-center'>
                    <div className='col-md-6'>
                    <h5 className='text-center'>Signin an account</h5>
                        <div className='card mt-3'>
                            <div className='card-body'>
                                <form className="">
                                    <div className="form-group ">
                                        <label>Email</label>
                                        <input
                                            type="email"
                                            onChange={handleChange("email")}
                                            value={email}
                                            name='email'
                                            className="form-control"
                                            placeholder="Enter username"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Password</label>
                                        <input
                                            type="password"
                                            name='password'
                                            onChange={handleChange("password")}
                                            value={password}
                                            className="form-control"
                                            placeholder="Enter password"/>
                                    </div>
                                    <div className='text-center mt-5 mb-4'>
                                        <button onClick={onSubmit} class="btn btn btn-block btn-outline-info ml-center">SignIn</button>
                                    </div>
                                    <p className='font-weight-bold'>Don't have an account</p>
                                    <div className='text-center mt-4 mb-3'>
                                        <Link to='/signup'>
                                            <button className="btn btn-block  ml-center">SignUp</button>
                                        </Link>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    return (<BaseScreen>
        {loadingMessage()}
      {errorMessage()}
      {signinForm()}
      {performRedirect()}
    </BaseScreen>)
}

export default SigninScreen
