const Product = require("../models/product");

exports.createProduct = (req, res) => {

  const product = new Product(req.body);
  // console.log('jkkksd',product)

    //save to the DB
    product.save((err, products) => {
      if (err) {
        res.status(400).json({
          error: "Saving tshirt in DB failed"+err
        });
      }
      res.json(products);
    });
  
};
//middleware
exports.photo = (req, res, next) => {
  if (req.product.photo.data) {
    res.set("Content-Type", req.product.photo.contentType);
    return res.send(req.product.photo.data);
  }
  next();
};



exports.getProducts = (req, res) => {

  Product.find().exec((err, products) => {
      if (err) {
        return res.status(400).json({
          error: "NO product FOUND"
        });
      }
      res.json(products);
    });
};